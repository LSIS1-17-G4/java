package ui;

/**
 *
 * @author Daniel
 */
import java.sql.Connection;
import controller.InscricaoEquipaController;
import utils.Utils;

public class InscricaoEquipaUI {
     private final InscricaoEquipaController controller;
    
    public InscricaoEquipaUI(Connection connector){
        controller = new InscricaoEquipaController(connector);
    }
    
    public void postInscricaoEquipa() throws Exception{
        
        int IDEquipa=Utils.IntFromConsole("Introduza o número identificador da Equipa: ");
        String nomeEquipa= Utils.readLineFromConsole("Introduza o nome da Equipa: ");
        String email= Utils.readLineFromConsole("Introduza o email da equipa: ");
        int contacto= Utils.IntFromConsole("Introduza o contacto da equipa: ");
        int idade= Utils.IntFromConsole("Introduza a idade media da equipa: ");
        
        controller.post(IDEquipa, nomeEquipa,email,contacto, idade);
    }
    
    public void deleteInscricaoEquipa() throws Exception{
        
        int IDEquipa=Utils.IntFromConsole("Indique o ID da equipa a eliminar: ");
        
        controller.delete(IDEquipa);
    }
    
    public void editInscricaoEquipa() throws Exception{
        int idProcura= Utils.IntFromConsole("Introduza o ID da equipa que pretende efetuar alterações: ");
        System.out.println("Introduza os novos dados");
        int IDEquipa=Utils.IntFromConsole("Número identificador da equipa: ");
        String nomeEquipa= Utils.readLineFromConsole("Nome da equipa: ");
        String email= Utils.readLineFromConsole("Email da equipa: ");
        int contacto= Utils.IntFromConsole("Contacto da equipa: ");
        int idade= Utils.IntFromConsole("Idade media da equipa: ");
        
        controller.update(idProcura, IDEquipa, nomeEquipa, email, contacto, idade);
        
    }
    
    public void run() throws Exception{
        run_in();
        run_edit();
        run_del();
    }
    public void run_in() throws Exception{
        postInscricaoEquipa();
    }
    
    public void run_edit() throws Exception{
        editInscricaoEquipa();
    }
    
    public void run_del() throws Exception{
        deleteInscricaoEquipa();
    }
}
