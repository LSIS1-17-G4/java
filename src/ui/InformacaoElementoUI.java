package ui;

/**
 *
 * @author Daniel
 */
import java.sql.Connection;
import controller.InformacaoElementoController;
import utils.Utils;

public class InformacaoElementoUI {
     private final InformacaoElementoController controller;
    
    public InformacaoElementoUI(Connection connector){
        controller = new InformacaoElementoController(connector);
    }
    
    public void postElemento() throws Exception{
        
        int IDElemento=Utils.IntFromConsole("Introduza o número identificador do elemento: ");
        String nomeElemento= Utils.readLineFromConsole("Introduza o nome do elemento: ");
        int idade= Utils.IntFromConsole("Introduza a idade do elemento: ");
        
        controller.post(IDElemento, nomeElemento, idade);
    }
    
    public void deleteElemento() throws Exception{
        
        int id=Utils.IntFromConsole("Indique o ID do elemento a eliminar: ");
        
        controller.delete(id);
    }
    
    public void editElemento() throws Exception{
        int idProcura= Utils.IntFromConsole("Introduza o ID do elemento que pretende efetuar alterações: ");
        System.out.println("Introduza os novos dados");
        int IDElemento=Utils.IntFromConsole("Número identificador do elemento: ");
        String nomeElemento= Utils.readLineFromConsole("Nome do elemento: ");
        int idade= Utils.IntFromConsole("Idade do elemento: ");
        
        controller.update(idProcura, IDElemento, nomeElemento, idade);
        
    }
    public void run_in() throws Exception{
        postElemento();
    }
    
    public void run_edit() throws Exception{
        editElemento();
    }
    
    public void run_del() throws Exception{
        deleteElemento();
    }
}
