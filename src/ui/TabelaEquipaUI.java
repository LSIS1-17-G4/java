package ui;

/**
 *
 * @author Daniel
 */
import java.sql.Connection;
import controller.TabelaEquipaController;
import utils.Utils;

public class TabelaEquipaUI {
    private final TabelaEquipaController controller;
    
    public TabelaEquipaUI(Connection connector){
        controller = new TabelaEquipaController(connector);
    }
    
    public void postTabelaEquipa() throws Exception{
        
        int IDEquipa=Utils.IntFromConsole("Introduza o ID Equipa: ");
        String nomeEquipa= Utils.readLineFromConsole("Introduza o nome da Equipa: ");
        String observacoes= Utils.readLineFromConsole("Introduza observacoes da Equipa: ");
        int numeroElementos= Utils.IntFromConsole("Introduza a numero de elementos: ");
        
        controller.post(IDEquipa, nomeEquipa, observacoes, numeroElementos);
    }
    
    public void deleteTabelaEquipa() throws Exception{
        
        int IDEquipa=Utils.IntFromConsole("Indique o numero da Equipa a eliminar: ");
        int numeroElementos=Utils.IntFromConsole("Indique o numero de elementos a eliminar: ");
        
        controller.delete(IDEquipa);
        controller.delete(numeroElementos);
    }
    
    public void editTabelaEquipa() throws Exception{
        int idProcura= Utils.IntFromConsole("Introduza o ID da Equipa que pretende efetuar alterações: ");
        System.out.println("Introduza os novos dados");
        int IDEquipa=Utils.IntFromConsole("ID da equipa: ");
        int numeroElementos=Utils.IntFromConsole("Número de elementos: ");
        String nomeEquipa= Utils.readLineFromConsole("Nome da Equipa: ");
        String observacoes = Utils.readLineFromConsole("\n Observacoes: ");
        
        controller.update(idProcura, IDEquipa, nomeEquipa, observacoes, numeroElementos);
        
    }
    
    public void run() throws Exception{
        run_in();
        run_edit();
        run_del();
    }
    public void run_in() throws Exception{
        postTabelaEquipa();
    }
    
    public void run_edit() throws Exception{
        editTabelaEquipa();
    }
    
    public void run_del() throws Exception{
        deleteTabelaEquipa();
    }
}
