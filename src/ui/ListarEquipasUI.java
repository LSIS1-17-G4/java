package ui;

/**
 *
 * @author Daniel
 */
import java.sql.Connection;
import controller.ListarEquipasController;
import utils.Utils;

public class ListarEquipasUI {
     private final ListarEquipasController controller;
    
    public ListarEquipasUI(Connection connector){
        controller = new ListarEquipasController(connector);
    }
    
    public void postListarEquipas() throws Exception{
        
        int numeroEquipa=Utils.IntFromConsole("Introduza o número da Equipa: ");
        
        controller.post(numeroEquipa);
    }
    
    public void deleteListarEquipas() throws Exception{
        
        int numeroEquipa=Utils.IntFromConsole("Indique o numero da Equipa a eliminar: ");
        
        controller.delete(numeroEquipa);
    }
    
    public void editListarEquipas() throws Exception{
        int idnumeroEquipa= Utils.IntFromConsole("Introduza o numero da Equipa que pretende efetuar alterações: ");
        System.out.println("Introduza os novos dados");
        int numeroEquipa=Utils.IntFromConsole("Número da Equipa: ");
        
        controller.update(idnumeroEquipa, numeroEquipa);
        
    }
    
    public void run() throws Exception{
        run_in();
        run_edit();
        run_del();
    }
    public void run_in() throws Exception{
        postListarEquipas();
    }
    
    public void run_edit() throws Exception{
        editListarEquipas();
    }
    
    public void run_del() throws Exception{
        deleteListarEquipas();
    }
}
