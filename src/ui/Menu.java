package ui;

import java.io.IOException;
import java.sql.Connection;
import utils.Utils;

public class Menu {
    
    private static Connection conn;
    private String opcao;
    private String escolha;
    
    public Menu(Connection conet) {
        conn= conet;
    }
    
    public void run() throws IOException, Exception {
        
        do {
            System.out.println("\n\n");
            System.out.println("1. Especificar a Equipa");
            System.out.println("2. Definir as Equipas");
            System.out.println("3. Definir a Informacao de Elemento");
            System.out.println("4. Definir a Informacao de Prova");
            System.out.println("5. Inscrever a Equipa");
            System.out.println("6. Definir a Informacao de Equipa");
            System.out.println("0. Sair");  
            
            opcao = Utils.readLineFromConsole("Introduza opção: ");

            if( opcao.equals("1") )
            {
                do{
                        System.out.println("\n\n");
                        System.out.println("1. Inserir");
                        System.out.println("2. Editar"); 
                        System.out.println("3. Apagar");
                        System.out.println("0. Sair");
            
            escolha = Utils.readLineFromConsole("Introduza escolha: ");
                 if ( escolha.equals("1") ){
                     TabelaEquipaUI ui = new TabelaEquipaUI(conn);
                     ui.run_in();
//                     break;
                }
                if ( escolha.equals("2") ){
                     TabelaEquipaUI ui = new TabelaEquipaUI(conn);
                     ui.run_edit();
//                     break;
                }
                if (escolha.equals("3") ){
                    TabelaEquipaUI ui = new TabelaEquipaUI(conn);
                    ui.run_del();
//                    break;
                }
//                break;
               }
                while (!opcao.equals("0") ); {
                break;
            }
            }
            
            if( opcao.equals("2") )
            {
                 do{
                        System.out.println("\n\n");
                        System.out.println("1. Inserir");
                        System.out.println("2. Editar"); 
                        System.out.println("3. Apagar");
                        System.out.println("0. Sair");
            
            escolha = Utils.readLineFromConsole("Introduza escolha: ");
                 if ( escolha.equals("1") ){
                     ListarEquipasUI ui = new ListarEquipasUI(conn);
                     ui.run_in(); 
//                     break;
                }
                if ( escolha.equals("2") ){
                     ListarEquipasUI ui = new ListarEquipasUI(conn);
                     ui.run_edit();
//                     break;
                }
                if (escolha.equals("3") ){
                    ListarEquipasUI ui = new ListarEquipasUI(conn);
                    ui.run_del();
//                    break;
                }
//                break;
               }
                while (!opcao.equals("0") ); {
            }
                break;
            }
            
            if( opcao.equals("3") ) {

               do{
                        System.out.println("\n\n");
                        System.out.println("1. Inserir");
                        System.out.println("2. Editar"); 
                        System.out.println("3. Apagar");
                        System.out.println("0. Sair");
            
            escolha = Utils.readLineFromConsole("Introduza escolha: ");
                 if ( escolha.equals("1") ){
                     InformacaoElementoUI ui = new InformacaoElementoUI(conn);
                     ui.run_in();
//                     break;
                }
                if ( escolha.equals("2") ){
                     InformacaoElementoUI ui = new InformacaoElementoUI(conn);
                     ui.run_edit();
//                     break;
                }
                if (escolha.equals("3") ){
                    InformacaoElementoUI ui = new InformacaoElementoUI(conn);
                    ui.run_del();
//                    break;
                }
//                break;
               }
                while (!opcao.equals("0") ); {
            }
                break;
            }
            
            if( opcao.equals("4") )
            {
                 do{
                        System.out.println("\n\n");
                        System.out.println("1. Inserir");
                        System.out.println("2. Editar"); 
                        System.out.println("3. Apagar");
                        System.out.println("0. Sair");
            
            escolha = Utils.readLineFromConsole("Introduza escolha: ");
                 if ( escolha.equals("1") ){
                     InformacaoProvaUI ui = new InformacaoProvaUI(conn);
                     ui.run_in();
//                     break;
                }
                if ( escolha.equals("2") ){
                     InformacaoProvaUI ui = new InformacaoProvaUI(conn);
                     ui.run_edit();
//                     break;
                }
                if (escolha.equals("3") ){
                   InformacaoProvaUI ui = new InformacaoProvaUI(conn);
                    ui.run_del();
//                    break;
                }
//                break;
               }
                while (!opcao.equals("0") ); {
            }
                break;
            }
            
            if( opcao.equals("5") )
            {
                 do{
                        System.out.println("\n\n");
                        System.out.println("1. Inserir");
                        System.out.println("2. Editar"); 
                        System.out.println("3. Apagar");
                        System.out.println("0. Sair");
            
            escolha = Utils.readLineFromConsole("Introduza escolha: ");
                 if ( escolha.equals("1") ){
                     InscricaoEquipaUI ui = new InscricaoEquipaUI(conn);
                     ui.run_in(); 
//                     break;
                }
                if ( escolha.equals("2") ){
                     InscricaoEquipaUI ui = new InscricaoEquipaUI(conn);
                     ui.run_edit(); 
//                     break;
                }
                if (escolha.equals("3") ){
                    InscricaoEquipaUI ui = new InscricaoEquipaUI(conn);
                    ui.run_del();
//                    break;
                }
//                break;
               }
                while (!opcao.equals("0") ); {
            }
                break;
            }
            
            if( opcao.equals("6") )
            {
                 do{
                        System.out.println("\n\n");
                        System.out.println("1. Inserir");
                        System.out.println("2. Editar"); 
                        System.out.println("3. Apagar");
                        System.out.println("0. Sair");
            
            escolha = Utils.readLineFromConsole("Introduza escolha: ");
                 if ( escolha.equals("1") ){
                     InformacaoEquipaUI ui = new InformacaoEquipaUI(conn);
                     ui.run_in(); 
//                     break;
                }
                     
                if ( escolha.equals("2") ){
                     InformacaoEquipaUI ui = new InformacaoEquipaUI(conn);
                     ui.run_edit(); 
//                     break;
                }
                if (escolha.equals("3") ){
                    InformacaoEquipaUI ui = new InformacaoEquipaUI(conn);
                    ui.run_del();
//                    break;
                }
//                 break;
               }
                while (!opcao.equals("0") ); {
            }
                break;
            }
        }
        while (!opcao.equals("0") );
        }
}