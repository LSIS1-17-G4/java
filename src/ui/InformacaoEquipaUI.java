package ui;

/**
 *
 * @author Daniel
 */
import java.sql.Connection;
import controller.InformacaoEquipaController;
import utils.Utils;

public class InformacaoEquipaUI {
     private final InformacaoEquipaController controller;
    
    public InformacaoEquipaUI(Connection connector){
        controller = new InformacaoEquipaController(connector);
    }
    
    public void postInformacaoEquipa() throws Exception{
        
        int IDEquipa=Utils.IntFromConsole("Introduza o número identificador da equipa: ");
        String nomeEquipa= Utils.readLineFromConsole("Introduza o nome da equipa: ");
        int numeroElementos= Utils.IntFromConsole("Introduza o numero de elementos da equipa: ");
        
        controller.post(IDEquipa, nomeEquipa, numeroElementos);
    }
    
    public void deleteInformacaoEquipa() throws Exception{
        
        int id=Utils.IntFromConsole("Indique o ID da equipa a eliminar: ");
        
        controller.delete(id);
    }
    
    public void editInformacaoEquipa() throws Exception{
        int idProcura= Utils.IntFromConsole("Introduza o ID da equipa que pretende efetuar alterações: ");
        System.out.println("Introduza os novos dados");
        int IDEquipa=Utils.IntFromConsole("Número identificador da equipa: ");
        String nomeEquipa= Utils.readLineFromConsole("Nome da equipa: ");
        int numeroElementos= Utils.IntFromConsole("Numero de Elementos: ");
        
        controller.update(idProcura, IDEquipa, nomeEquipa, numeroElementos);
        
    }
    
    public void run() throws Exception{
        run_in();
        run_edit();
        run_del();
    }
    public void run_in() throws Exception{
        postInformacaoEquipa();
    }
    public void run_edit() throws Exception{
        editInformacaoEquipa();
    }
    public void run_del() throws Exception{
        deleteInformacaoEquipa();
    }
}