package ui;

import utils.database;
import java.sql.Connection;

public class Main {

    public static Connection conn;

    public static void main(String[] args) {
     try
        {
            conn = database.getConnection();
             Menu uiMenu= new Menu(conn);
             
             uiMenu.run();
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
    }
}