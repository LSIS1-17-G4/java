package ui;

/**
 *
 * @author Daniel
 */
import java.sql.Connection;
import controller.InformacaoProvaController;
import utils.Utils;

public class InformacaoProvaUI {
    private final InformacaoProvaController controller;
    
    public InformacaoProvaUI(Connection connector){
        controller = new InformacaoProvaController(connector);
    }
    
    public void postProva() throws Exception{
        
        int IDProva=Utils.IntFromConsole("Introduza o número identificador da Prova: ");
        String nomeProva= Utils.readLineFromConsole("Introduza o nome da prova: ");
        
        controller.post(IDProva, nomeProva);
    }
    
    public void deleteProva() throws Exception{
        
        int id=Utils.IntFromConsole("Indique o ID da Prova a eliminar: ");
        
        controller.delete(id);
    }
    
    public void editProva() throws Exception{
        int idProcura= Utils.IntFromConsole("Introduza o ID da Prova que pretende efetuar alterações: ");
        System.out.println("Introduza os novos dados");
        int IDProva=Utils.IntFromConsole("Número identificador da Prova: ");
        String nomeProva= Utils.readLineFromConsole("Nome da Prova: ");
        
        controller.update(idProcura, IDProva, nomeProva);
        
    }
    
    public void run() throws Exception{
        run_in();
        run_edit();
        run_del();
    }
    public void run_in() throws Exception{
        postProva();
    }
    
    public void run_edit() throws Exception{
        editProva();
    }
    
    public void run_del() throws Exception{
        deleteProva();
    }
}
