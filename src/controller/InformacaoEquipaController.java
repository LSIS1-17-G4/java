package controller;

/**
 *
 * @author Daniel
 */
import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.InformacaoEquipa;

public class InformacaoEquipaController {
    
    static Connection conn;
    
    public InformacaoEquipaController(Connection conexao){
        
        conn = conexao;
    }
    
    public void post(int ID, String nome, int numeroElementos) throws Exception{
     
	try {
            
	PreparedStatement posted = (PreparedStatement) conn.prepareStatement("INSERT INTO `informacaoequipa` (`IDEquipa`,`nomeEquipa`, `numeroElementos`) VALUES ('"+ID+"','"+nome+"', '"+numeroElementos+"')");
	posted.executeUpdate();
	} catch(Exception e) {
	System.out.println(e);}
	finally {
            System.out.println("Informação inserida");
}
}
    
    public void update(int idprocura, int ID, String nome, int numeroElementos) throws Exception{
        
        try {
            
           PreparedStatement update= (PreparedStatement) conn.prepareStatement("UPDATE `InformacaoEquipa` SET `IDEquipa`='"+ID+"',`nomeEquipa`='"+nome+"',`numeroElementos`='"+numeroElementos+"' WHERE `IDElemento`='"+idprocura+"'");
           
           update.executeUpdate();
        } catch (Exception e){
            System.out.println(e);}
        finally {
            System.out.println("Informação atualizada");
        }
    }
    
    public void delete(int idprocura) throws Exception{ 
        try {

           PreparedStatement update= (PreparedStatement) conn.prepareStatement("DELETE FROM `InformacaoEquipa` WHERE `IDEquipa`='"+idprocura+"'");
           
           update.executeUpdate();
        } catch (Exception e){
            System.out.println(e);}
        finally {
            System.out.println("Elemento apagado");
        }
}
}