package controller;

/**
 *
 * @author Daniel
 */
import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.InscricaoEquipa;

public class InscricaoEquipaController {

    static Connection conn;
    
    public InscricaoEquipaController(Connection conexao){
        
        conn = conexao;
    }
    
    public void post(int ID, String nome, String email, int contacto, int idade) throws Exception{
     
	try {
            
	PreparedStatement posted = (PreparedStatement) conn.prepareStatement("INSERT INTO `inscricaoequipa` (`IDEquipa`,`nomeEquipa`, `email`,`contacto`, `idade`) VALUES ('"+ID+"','"+nome+"','"+email+"','"+contacto+"','"+idade+"')");
	posted.executeUpdate();
	} catch(Exception e) {
	System.out.println(e);}
	finally {
            System.out.println("Equipa inserida");
}
}
    
    public void update(int idprocura, int ID, String nome,String email, int contacto, int idade) throws Exception{
        
        try {
            
           PreparedStatement update= (PreparedStatement) conn.prepareStatement("UPDATE `InscricaoEquipa` SET `IDEquipa`='"+ID+"',`nomeEquipa`='"+nome+"',`email`='"+email+"',`contacto`='"+contacto+"', `idade`='"+idade+"' WHERE `IDEquipa`='"+idprocura+"'");
           
           update.executeUpdate();
        } catch (Exception e){
            System.out.println(e);}
        finally {
            System.out.println("Inscrição atualizada");
        }
    }
    
    public void delete(int idprocura) throws Exception{ 
        try {

           PreparedStatement update= (PreparedStatement) conn.prepareStatement("DELETE FROM `InscricaoEquipa` WHERE `IDEquipa`='"+idprocura+"'");
           
           update.executeUpdate();
        } catch (Exception e){
            System.out.println(e);}
        finally {
            System.out.println("Equipa apagada");
        }
}
}