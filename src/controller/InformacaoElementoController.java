package controller;

/**
 *
 * @author Daniel
 */
import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.sql.SQLException;
import model.InformacaoElemento;

public class InformacaoElementoController {
    
    static Connection conn;
    
    public InformacaoElementoController(Connection conexao){
        
        conn = conexao;
    }
    
    public void post(int IDElemento, String nomeElemento, int idade) throws Exception{
     
	try {
            
	PreparedStatement posted = (PreparedStatement)conn.prepareStatement("INSERT INTO elemento (IDElemento,nomeElemento,idade) VALUES ("+IDElemento+",'"+nomeElemento+"', "+idade+")");
        if(posted==null)
            System.out.println("Sou nulo");

//System.out.println("INSERT INTO `elemento` (`IDElemento`,`nomeElemento`, `idade`) VALUES ('"+IDElemento+"','"+nomeElemento+"', '"+idade+"')");
        posted.executeUpdate();
	} catch(SQLException e) {
	System.out.println(e.getSQLState());}
	finally {
            System.out.println("Elemento inserido");
}
}
    
    public void update(int idprocura, int ID, String nome, int idade) throws Exception{
        
        try {
            
           PreparedStatement update= (PreparedStatement) conn.prepareStatement("UPDATE `elemento` SET `IDElemento`='"+ID+"',`nomeElemento`='"+nome+"',`idade`='"+idade+"' WHERE `IDElemento`='"+idprocura+"'");
           
           update.executeUpdate();
        } catch (Exception e){
            System.out.println(e);}
        finally {
            System.out.println("Elemento atualizado");
        }
    }
    
    public void delete(int idprocura) throws Exception{ 
        try {

           PreparedStatement update= (PreparedStatement) conn.prepareStatement("DELETE FROM `elemento` WHERE `IDElemento`='"+idprocura+"'");
           
           update.executeUpdate();
        } catch (Exception e){
            System.out.println(e);}
        finally {
            System.out.println("Elemento apagado");
        }
}
}
