package controller;

/**
 *
 * @author Daniel
 */
import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.TabelaEquipa;

public class TabelaEquipaController {
    
    static Connection conn;
    
    public TabelaEquipaController(Connection conexao){
        
        conn = conexao;
    }
    
    public void post(int ID, String nomeEquipa, String observacoes, int numeroElementos) throws Exception{
     
	try {
            
	PreparedStatement posted = (PreparedStatement) conn.prepareStatement("INSERT INTO `tabelaequipa` (`IDEquipa`,`nomeEquipa`, `observacoes` , `numeroElementos`) VALUES ('"+ID+"','"+nomeEquipa+"', '"+observacoes+"','"+numeroElementos+"')");
	posted.executeUpdate();
	} catch(Exception e) {
	System.out.println(e);}
	finally {
            System.out.println("Equipa inserida");
}
}
    
    public void update(int idprocura, int ID, String nomeEquipa, String observacoes, int numeroElementos) throws Exception{
        
        try {
            
           PreparedStatement update= (PreparedStatement) conn.prepareStatement("UPDATE `tabelaequipa` SET `IDEquipa`='"+ID+"',`nomeEquipa`='"+nomeEquipa+"',`observacoes`='"+observacoes+"', `numeroElementos`='"+numeroElementos+"' WHERE `IDEquipa`='"+idprocura+"'");
           
           update.executeUpdate();
        } catch (Exception e){
            System.out.println(e);}
        finally {
            System.out.println("Equipa atualizada");
        }
    }
    
    public void delete(int idprocura) throws Exception{ 
        try {

           PreparedStatement update= (PreparedStatement) conn.prepareStatement("DELETE FROM `tabelaequipa` WHERE `IDEquipa`='"+idprocura+"'");
           
           update.executeUpdate();
        } catch (Exception e){
            System.out.println(e);}
        finally {
            System.out.println("Equipa apagada");
        }
}
}