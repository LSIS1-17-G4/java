package controller;

/**
 *
 * @author Daniel
 */
import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.InformacaoProva;

public class InformacaoProvaController {
    
    static Connection conn;
    
    public InformacaoProvaController(Connection conexao){
        
        conn = conexao;
    }
    
    public void post(int ID, String nome) throws Exception{
     
	try {
            
	PreparedStatement posted = (PreparedStatement) conn.prepareStatement("INSERT INTO `Prova` (`IDProva`,`nomeProva`) VALUES ('"+ID+"','"+nome+"')");
	posted.executeUpdate();
	} catch(Exception e) {
	System.out.println(e);}
	finally {
            System.out.println("Informação Prova inserida");
}
}
    
    public void update(int idprocura, int ID, String nome) throws Exception{
        
        try {
            
           PreparedStatement update= (PreparedStatement) conn.prepareStatement("UPDATE `Prova` SET `IDProva`='"+ID+"',`nomeProva`='"+nome+"' WHERE `IDProva`='"+idprocura+"'");
           
           update.executeUpdate();
        } catch (Exception e){
            System.out.println(e);}
        finally {
            System.out.println("Informação Prova atualizada");
        }
    }
    
    public void delete(int idprocura) throws Exception{ 
        try {

           PreparedStatement update= (PreparedStatement) conn.prepareStatement("DELETE FROM `Prova` WHERE `IDProva`='"+idprocura+"'");
           
           update.executeUpdate();
        } catch (Exception e){
            System.out.println(e);}
        finally {
            System.out.println("Informação Prova apagada");
        }
}
}