package controller;

/**
 *
 * @author Daniel
 */
import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.ListarEquipas;

public class ListarEquipasController {

    static Connection conn;
    
    public ListarEquipasController(Connection conexao){
        
        conn = conexao;
    }
    
    public void post(int numeroEquipa) throws Exception{
     
	try {
            
	PreparedStatement posted = (PreparedStatement) conn.prepareStatement("INSERT INTO `listarequipas` (`numeroEquipa`) VALUES ('"+numeroEquipa+"')");
	posted.executeUpdate();
	} catch(Exception e) {
	System.out.println(e);}
	finally {
            System.out.println("Equipa listada");
}
}
    
    public void update(int idnumeroEquipa, int numeroEquipa) throws Exception{
        
        try {
            
           PreparedStatement update= (PreparedStatement) conn.prepareStatement("UPDATE `ListarEquipas` SET `numeroEquipa`='"+numeroEquipa+"' WHERE `numeroEquipa`='"+idnumeroEquipa+"'");
           
           update.executeUpdate();
        } catch (Exception e){
            System.out.println(e);}
        finally {
            System.out.println("Equipa listada atualizada");
        }
    }
    
    public void delete(int idprocura) throws Exception{ 
        try {

           PreparedStatement update= (PreparedStatement) conn.prepareStatement("DELETE FROM `ListarEquipas` WHERE `numeroEquipa`='"+idprocura+"'");
           
           update.executeUpdate();
        } catch (Exception e){
            System.out.println(e);}
        finally {
            System.out.println("Equipa listada apagada");
        }
}
}
