package model;

/**
 *
 * @author Daniel
 */
public class TabelaEquipa {
    private int m_IDEquipa;
    private String m_nomeEquipa;
    private String m_observacoes;
    private int m_numeroElementos;
    
    public TabelaEquipa() {
    }
    
    public TabelaEquipa(int m_IDEquipa, String m_nomeEquipa,String m_observacoes, int m_numeroElementos){
        this.m_IDEquipa=m_IDEquipa;
        this.m_nomeEquipa=m_nomeEquipa;
        this.m_observacoes=m_observacoes;
        this.m_numeroElementos= m_numeroElementos;
    }

    public int getM_IDEquipa() {
        return m_IDEquipa;
    }

    public void setM_IDEquipa(int m_IDEquipa) {
        this.m_IDEquipa = m_IDEquipa;
    }

    public String getM_nomeEquipa() {
        return m_nomeEquipa;
    }

    public void setM_nomeEquipa(String m_nomeEquipa) {
        this.m_nomeEquipa = m_nomeEquipa;
    }
    
    public String getM_observacoes() {
        return m_observacoes;
    }

    public void setM_observacoes(String m_observacoes) {
        this.m_observacoes = m_observacoes;
    }
    
    public int getM_numeroElementos() {
        return m_numeroElementos;
    }

    public void setM_numeroElementos(int m_numeroElementos) {
        this.m_numeroElementos = m_numeroElementos;
    }

    @Override
    public String toString() {
        return "Equipa: " + "numero Equipa: " + m_IDEquipa + "nome Equipa: " + m_nomeEquipa + "observacoes: " + m_observacoes + " numero Elementos: " + m_numeroElementos + '}';
    } 
}
