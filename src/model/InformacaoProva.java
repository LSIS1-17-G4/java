package model;

/**
 *
 * @author Daniel
 */
public class InformacaoProva {
    private int m_IDProva;
    private String m_nomeProva;
    
    public InformacaoProva() {
    }
    
    public InformacaoProva(int m_IDProva, String m_nomeProva){
        this.m_IDProva=m_IDProva;
        this.m_nomeProva=m_nomeProva;
    }

    public int getM_IDProva() {
        return m_IDProva;
    }

    public void setM_IDProva(int m_IDProva) {
        this.m_IDProva = m_IDProva;
    }

    public String getM_nomeProva() {
        return m_nomeProva;
    }

    public void setM_nomeProva(String m_nomeProva) {
        this.m_nomeProva = m_nomeProva;
    }

    @Override
    public String toString() {
        return "Elemento: " + "ID: " + m_IDProva + "nome: " + m_nomeProva + '}';
    } 
}
