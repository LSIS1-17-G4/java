package model;

/**
 *
 * @author Daniel
 */
public class InformacaoElemento {
    private int m_IDElemento;
    private String m_nomeElemento;
    private int m_idade;
    
    public InformacaoElemento() {
    }
    
    public InformacaoElemento(int m_IDElemento, String m_nomeElemento, int m_idade){
        this.m_IDElemento=m_IDElemento;
        this.m_nomeElemento=m_nomeElemento;
        this.m_idade= m_idade;
    }

    public int getM_IDElemento() {
        return m_IDElemento;
    }

    public void setM_IDElemento(int m_IDElemento) {
        this.m_IDElemento = m_IDElemento;
    }

    public String getM_nomeElemento() {
        return m_nomeElemento;
    }

    public void setM_nomeElemento(String m_nomeElemento) {
        this.m_nomeElemento = m_nomeElemento;
    }

    public int getM_idade() {
        return m_idade;
    }

    public void setM_idade(int m_idade) {
        this.m_idade = m_idade;
    }

    @Override
    public String toString() {
        return "Elemento: " + "ID: " + m_IDElemento + "nome: " + m_nomeElemento + " idade: " + m_idade + '}';
    } 
}
