package model;

/**
 *
 * @author Daniel
 */
public class ListarEquipas {
    private int m_numeroEquipa;
    
    public ListarEquipas() {
    }
    
    public ListarEquipas(int m_numeroEquipa){
        this.m_numeroEquipa=m_numeroEquipa;
    }

    public int getM_numeroEquipa() {
        return m_numeroEquipa;
    }

    public void setM_numeroEquipa(int m_numeroEquipa) {
        this.m_numeroEquipa = m_numeroEquipa;
    }

    @Override
    public String toString() {
        return "Numero da Equipa: " + "numero da Equipa: " + m_numeroEquipa + '}';
    } 
}