package model;

/**
 *
 * @author Daniel
 */
public class InscricaoEquipa {
    private int m_IDEquipa;
    private String m_nomeEquipa;
    private String m_email;
    private int m_contacto;
    private int m_idade;
    
    public InscricaoEquipa() {
    }
    
    public InscricaoEquipa(int m_IDEquipa, String m_nomeEquipa,String m_email,int m_contacto, int m_idade){
        this.m_IDEquipa=m_IDEquipa;
        this.m_nomeEquipa=m_nomeEquipa;
        this.m_email=m_email;
        this.m_contacto=m_contacto;
        this.m_idade= m_idade;
    }

    public int getM_IDEquipa() {
        return m_IDEquipa;
    }

    public void setM_IDEquipa(int m_IDEquipa) {
        this.m_IDEquipa = m_IDEquipa;
    }

    public String getM_nomeEquipa() {
        return m_nomeEquipa;
    }

    public void setM_nomeEquipa(String m_email) {
        this.m_email = m_email;
    }
    
     public String getM_email() {
        return m_nomeEquipa;
    }

    public void setM_email(String m_email) {
        this.m_email = m_email;
    }
    
    public int getM_contacto() {
        return m_contacto;
    }

    public void setM_contacto(int m_contacto) {
        this.m_contacto = m_contacto;
    }
    
    public int getM_idade() {
        return m_idade;
    }

    public void setM_idade(int m_idade) {
        this.m_idade = m_idade;
    }

    @Override
    public String toString() {
        return "Inscricao: " + "ID: " + m_IDEquipa + "nome: " + m_nomeEquipa + " email: " + m_email + "contacto: " + m_contacto +  " idade: " + m_idade +'}';
    }  
}