package model;

/**
 *
 * @author Daniel
 */


public class InformacaoEquipa {
    private int m_IDEquipa;
    private String m_nomeEquipa;
    private int m_numeroElementos;
    
    public InformacaoEquipa() {
    }
    
    public InformacaoEquipa(int m_IDEquipa, String m_nomeEquipa, int m_numeroElementos){
        this.m_IDEquipa=m_IDEquipa;
        this.m_nomeEquipa=m_nomeEquipa;
        this.m_numeroElementos= m_numeroElementos;
    }

    public int getM_IDEquipa() {
        return m_IDEquipa;
    }

    public void setM_IDEquipa(int m_IDEquipa) {
        this.m_IDEquipa = m_IDEquipa;
    }

    public String getM_nomeEquipa() {
        return m_nomeEquipa;
    }

    public void setM_nomeEquipa(String m_nomeEquipa) {
        this.m_nomeEquipa = m_nomeEquipa;
    }

    public int getM_numeroElementos() {
        return m_numeroElementos;
    }

    public void setM_numeroElementos(int m_numeroElementos) {
        this.m_numeroElementos = m_numeroElementos;
    }

    @Override
    public String toString() {
        return "Elemento: " + "ID: " + m_IDEquipa + "nome: " + m_nomeEquipa + " numeroElementos: " + m_numeroElementos + '}';
    } 
}
